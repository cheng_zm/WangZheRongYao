const mongoose = require('mongoose')

const schema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    items: [{
        image: {
            type: String
        },
        title: {
            type: String
        },
        url: {
            type: String,
            required: true
        },
    }]

})

module.exports = mongoose.model('Ad', schema)