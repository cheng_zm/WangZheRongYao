module.exports = {
    //输出的文件夹路径
    outputDir: __dirname + '/../server/public/web',
    // 配置编译生成的斜杠是'/admin'，否则就是'/'


    //? '/'直接生成到跟地址，打开就直接移动端首页，等同于不要这个配置
    publicPath: process.env.NODE_ENV === 'production'
    
        ? '/'
        : '/'
}
